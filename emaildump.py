#!/usr/bin/python
#
#   Script to dump emails from BitBucket with repo invitiations, I put these
# messages into a lable using Gmail filters.
# I authenticate to gmail with oauth, which you can generate on the Google
# developer console and export into a JSON.
# Google API source can be found here:
#   https://code.google.com/p/google-mail-oauth2-tools/wiki/OAuth2DotPyRunThrough
# I used the script from that site to generate the refresh token, then I put it
# a JSON to use in the applicaiton.
#

import json
import email
from pprint import pprint
import base64
import imaplib
import googleOauth2 as googleAuth


def connect():
    pass_file = open('secrets.json','r')
    pass_json = json.load(pass_file);
    pass_file.close()
    refresh_file = open('refresh_token.json','r')
    refresh_json = json.load(refresh_file);
    refresh_file.close()
    
    token = googleAuth.RefreshToken(pass_json['installed']['client_id'], 
        pass_json['installed']['client_secret'],
        refresh_json['refresh_token'])
    
    auth_string = googleAuth.GenerateOAuth2String(pass_json['installed']['client_email'],
        token['access_token'],
        base64_encode=False)

    print
    imap_conn = imaplib.IMAP4_SSL('imap.gmail.com')
    imap_conn.debug = 4
    imap_conn.authenticate('XOAUTH2', lambda x: auth_string)
    # My label for  a filter of bitbucket emails
    imap_conn.select('bitbucket')

    return imap_conn

def getRepoInfo():
    conn = connect()
    result, data = conn.uid('search', None, "ALL")
    email_uids = data[0].split()
    submission_list = []
    for uid in email_uids:
        result, mail= conn.uid('fetch', uid, '(RFC822)')
        raw_email = mail[0][1]
        message = email.message_from_string(raw_email)
        submission_list.append((email.utils.parseaddr(message['From'])[0], message['Subject'].split()[-1]))
    
    submission_list = sorted(submission_list, key=lambda submission: submission[0].lower())
    return submission_list

