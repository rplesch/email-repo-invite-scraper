#!/usr/bin/bash

basedir=mp3/
currentdir=`pwd`

for repo in $basedir/*/*/; do
    cd $repo
    git add grade.txt
    git commit -m "Added grade"
    git push origin master
    cd $currentdir
done
