#!/usr/bin/python
#
#   Copies Git repos that are returned by getRepoInfo() in email-dump.py
# git module is not installed by default on but is easy to find.
# I have ssh configured on BitBucket with an RSA key pair, so it doesn't prompt
# me for a password.

import os
import git
import time
import emaildump
from subprocess import call


submission_list = emaildump.getRepoInfo()
outputfile = open('submissions'+time.strftime("%Y%m%d%H%M%S")+'.txt','w')
logfile = open('log'+time.strftime("%Y%m%d%H%M%S")+'.txt','w')

base_directory = os.getcwd()

for submission in submission_list:
    dirname = submission[1].split('/')[0]
    try:
        os.makedirs(dirname)
    except OSError:
        print "Couldn't create directory"
    os.chdir(dirname)

    try:
        git.Git().clone('git@bitbucket.org:' + submission[1] + '.git')
        call(['cp',base_directory+'/../grade.txt',base_directory+'/'+submission[1]+'/grade.txt'])
    except git.exc.GitCommandError:
        os.chdir(base_directory)
        err =  'WARNING: could not clone repo: git@bitbucket.org:' + submission[1] + '.git'
        print err
        logfile.write(err + '\n')

    os.chdir(base_directory)
    outputfile.write(submission[0] + '\t' + submission[1] + '\n')

outputfile.close()
logfile.close()
